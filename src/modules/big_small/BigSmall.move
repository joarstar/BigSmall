address 0x91DCf3413f09389FdE32388C85980aAC {
module BigSmall {
    use 0x1::Option::{Self, Option};
    use 0x1::Token::{Self, Token};
    use 0x1::STC::STC;
    use 0x1::Signer;
    use 0x1::Account;

    struct MyValue has store {
        value: u128,
        account_address: address,
        deposit: Token<STC>,
    }

    struct FirstValue has key,store {
        first: Option<MyValue>,
    }

    public fun init(account: &signer) {
        let addr = Signer::address_of(account);
        if (!exists<FirstValue>(addr)){
            move_to(account, FirstValue {
                first: Option::none(),
            });
        };
    }

    fun can_play(game_address: address) : bool {
        exists<FirstValue>(game_address)
    }

    fun is_new(game_address: address): bool acquires FirstValue {
        let first_value = borrow_global<FirstValue>(game_address);
        Option::is_none<MyValue>(&first_value.first)
    }

    public fun play(account: &signer, game_address: address, my_value: u128, my_deposit: Token<STC>) acquires FirstValue {
        assert(Self::can_play(game_address), 30000);
        let stc = Token::value<STC>(&my_deposit);
        assert(stc > 0 , 30001);
        let my_address = Signer::address_of(account);
        let is_new = Self::is_new(game_address);
        let first_value = borrow_global_mut<FirstValue>(game_address);
        if (is_new) {
            let first = MyValue {
                value: my_value,
                account_address: my_address,
                deposit: my_deposit,
            };
            Option::fill<MyValue>(&mut first_value.first, first);
        } else {
            assert(stc >= Token::value<STC>(&Option::borrow<MyValue>(&first_value.first).deposit), 30002);
            let first = Option::extract<MyValue>(&mut first_value.first);
            let MyValue {value: first_value, account_address: first_address, deposit: first_deposit} = first;
            if (my_value == first_value) {
                Account::deposit(first_address, first_deposit);
                Account::deposit(my_address, my_deposit);
            } else {
                let deposit = Token::join<STC>(first_deposit, my_deposit);
                if (my_value > first_value) {
                    Account::deposit(my_address, deposit);
                } else {
                    Account::deposit(first_address, deposit);
                }
            }
        }
    }
}
}
